---
title: ""
featured_image: '/img/banner.png'
description: "Fokuser på gaming med krystal klar lyd fra Teamspeak!"
---

Velkommen til Focus On Gaming sin nettside, her finner du blogposten våres og andre ting vi gjør,  

Vi skal og prøve å ligge ut status oppdateringer her hvis det er noe som skjer på Teamspeak serveren vår.  

Du kan koble til teamspeak server vår her: [ts3.focusongaming.no](https://invite.teamspeak.com/ts3.focusongaming.no/)
