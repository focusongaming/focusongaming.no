---
title: "Opprydning av kanaler som ikke brukes"
date: 2021-01-26
tags: ["ts3", "opprydning", "kanaler"]
---

## Vi rydder opp kanaler som ikke brukes
Nå er det snart 2 år siden vi gjorde en opprydning av kanaler på Teamspeak serveren vår, og siden den tid har det kommet mange flotte folk innom og laget seg kanaler.
Det er på tide med en ny runde med opprydning av kanaler og fjerne såkalte "ghostrooms".

Du har sikkert fått med deg at alle kanaler har fått et søppelspann på seg. 
Dette betyr at rommet er markert for sletting.
Hvis rommet ditt har et søppelspann på seg og du vil forsatt ha kanalen, så kan du sende en e-post til [support@focusongaming.no](mailto:support@focusongaming.no) med kanal navn og ditt teamspeak navn

NB! Hvis det er et søppelspann ikon på din etter 1.mars, blir kanalen slettet. Om du ber om en ny kanal etter fristen, vil den desverre lande under kanaler som ikke slettes.

evt bruk kontaktskjemaet under.
{{< form-contact action="https://formspree.io/f/xzbkooyb"  >}}