---
title: Kontakt oss
featured_image: "/img/notebook.jpg"
omit_header_text: true
description: We'd love to hear from you
type: page
menu: main

---

Har du noe spørsmål eller ider til oss så er det bare å sende en melding til oss via formen under, evt kan du sende mail til oss personlig

* Eiere
  
  * Xaner4: xaner4@focusongaming.no
  
  * Takkekort: takkekort@focusongaming.no

* Admin
  
  * Liffen: liffen@focusongaming.no

{{< form-contact action="https://formspree.io/xzbjqeya"  >}}
