---
title: "Om oss"

---

Focus on Gaming (FOG) var startet av en liten gjeng gamere utenfor 
bergen i 2013, Vi startet gruppen orginalt bare for et navn på 
Battlefield 4 gruppen vår. Ikke lenge etter startet vi med å laste opp 
videoer på YouTube under samme navn, men det ble aldri noe sukksess. 
Rundt samme tid som vi startet med Youtube, lagde vi også en Teamspeak 
server for å gjøre det lettere å ta opp krystall klar lyd fra alle som 
var med å spille. Denne startet vi som en privat selv hostet server, men
 etterhvert så vi at mangen brukte serveren ganske hyppig, vi steg fra 
et par stykk online til å fylle alle de 32 slot'ene på serveren hver 
kveld. Vi søkte etter Non-Profit lisens og fikk det tildelt fort, etter 
at vi hadde økt serveren fra 32 slot's til 512 slot's økte populariteten
 og det ble flere folk som brukte serveren, på det høyeste var vi nesten
 150 stykk på samtidig.  

I 2016 skilte en liten del av FOG seg ut og startet opp Norsk Utryknings 
klan (NUK) som forsatt er administrert av folk fra FOG. NUK var en 
rollespill klan for GTA 5 der fokus var på å rollespille de norske 
nødetatene. NUK var et vellykket prosjekt med omlag 100 spillere i 
helgene som stilte opp for å redde Los Santos.
